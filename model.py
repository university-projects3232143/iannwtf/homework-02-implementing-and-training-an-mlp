import numpy as np
import tensorflow as tf
from keras import Model, layers, losses, optimizers


class MyModel(Model):
    def __init__(self):
        super().__init__()
        self.layer1 = layers.Dense(256, activation=tf.nn.sigmoid)
        self.layer2 = layers.Dense(256, activation=tf.nn.sigmoid)
        self.output_layer = layers.Dense(10, activation=tf.nn.softmax)

    @tf.function
    def call(self, inputs):
        x = self.layer1(inputs)
        x = self.layer2(x)
        x = self.output_layer(x)
        return x

    def train(self,
              train_ds,
              test_ds,
              epochs_number: int = 10,
              loss_function=losses.CategoricalCrossentropy(),
              optimizer=optimizers.SGD()):

        train_losses = []
        train_accuracies = []
        test_losses = []
        test_accuracies = []

        for epoch in range(epochs_number):
            train_loss, train_accuracy = self.train_step_(train_ds, loss_function, optimizer)
            train_losses.append(train_loss)
            train_accuracies.append(train_accuracy)

            test_loss, test_accuracy = self.test(test_ds, loss_function)
            test_losses.append(test_loss)
            test_accuracies.append(test_accuracy)

            print(str(epoch+1)+"/"+str(epochs_number)+" epochs finished")

        return train_losses, train_accuracies, test_losses, test_accuracies

    def train_step_(self, train_ds, loss_function, optimizer):

        losses = []
        accuracies = []

        for (inp, target) in train_ds:
                with tf.GradientTape() as tape:
                    prediction = self(inp)
                    loss = loss_function(prediction, target)

                gradients = tape.gradient(loss, self.trainable_variables)
                optimizer.apply_gradients(zip(gradients, self.trainable_variables))
                train_accuracy = np.mean(np.argmax(prediction, axis=1) == np.argmax(target, axis=1))
                losses.append(loss)
                accuracies.append(train_accuracy)

        return tf.reduce_mean(losses), tf.reduce_mean(accuracies)

    def test(self, test_ds, loss_function):
        test_losses = []
        test_accuracies = []
        for (inp, target) in test_ds:
            prediction = self(inp)
            loss = loss_function(prediction, target)
            test_accuracy = np.mean(np.argmax(prediction, axis=1) == np.argmax(target, axis=1))
            test_losses.append(loss)
            test_accuracies.append(test_accuracy)

        return tf.reduce_mean(test_losses), tf.reduce_mean(test_accuracies)


