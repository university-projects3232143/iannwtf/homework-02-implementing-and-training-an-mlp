import tensorflow as tf
import tensorflow_datasets as tfds
from keras import optimizers

from model import MyModel
from visualization import visualize




def prepare_mnist_data(mnist):
    # convert data from uint8 to float32
    mnist = mnist.map(lambda img, target: (tf.cast(img, tf.float32), target))
    #flatten the images into vectors
    mnist = mnist.map(lambda img, target: (tf.reshape(img, (-1,)), target))
    #sloppy input normalization, just bringing image values from range [0, 255] to [-1, 1]
    mnist = mnist.map(lambda img, target: ((img/128.)-1., target))
    #create one-hot targets
    mnist = mnist.map(lambda img, target: (img, tf.one_hot(target, depth=10)))
    #cache this progress in memory, as there is no need to redo it; it is deterministic after all mnist mnist.cache()
    #shuffle, batch, prefetch
    mnist = mnist.shuffle (1000)
    mnist = mnist.batch(32)
    mnist.prefetch(20)
    #return preprocessed dataset
    return mnist


if __name__ == "__main__":
    (train_ds, test_ds), ds_info = tfds.load('mnist', split=['train', 'test'], as_supervised=True, with_info=True)
    train_ds = prepare_mnist_data(train_ds)
    test_ds = prepare_mnist_data(test_ds)

    model = MyModel()
    (train_losses,
     train_accuracies,
     test_losses,
     test_accuracies) = model.train(train_ds,
                                    test_ds,
                                    epochs_number=10,
                                    optimizer=optimizers.SGD(learning_rate=0.01, momentum=0.0))
    visualize(train_losses, train_accuracies, test_losses, test_accuracies)

